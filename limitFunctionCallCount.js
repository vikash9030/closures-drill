function limitFunctionCallCount(cb, n) {

    let count = 0;

    return function (a, b) {
        if (count < n) {
            count = count + 1
            return cb(a, b)
        }
        else {
            return null;
        }

    }


}

