function cacheFunction(cb) {

  const cache = {}

  return function (a, b , c, d) {

      if (!cache[`${a},${b},${c},${d}`]) {

          cache[`${a},${b},${c},${d}`] = cb(a, b ,c,d)
          return cb(a, b ,c,d)
      }
      else {

          return cache[`${a},${b} ,${c},{d}`]
      }
  }

}

module.exports = cacheFunction;
