const cacheFunction = require("../cacheFunction");

function add(a,b,c,d){
    if(c === undefined){
        return a+b;
     }else if(d === undefined){
         return a+b+c;
     }else{
         return a+b+c+d;
     }
}

const result = cacheFunction(add);

console.log(result(1,2 ,4));
console.log(result(1,3));
console.log(result(3,4));
console.log(result('vikash',' kumar', ' patel'));
console.log(result('vikash',' patel'));
