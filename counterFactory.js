function counterFactory(){

    let counter = 0;
    
    return{

        increment() { 
            counter = counter + 1
            return counter ;
        },
        decrement () { 
            counter = counter -1
            return counter ;
        }

    }
}

module.exports = counterFactory;
